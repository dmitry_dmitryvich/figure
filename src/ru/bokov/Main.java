package ru.bokov;

/**
 * Демо класс, предназначенный для демонстрации класса Figure
 *
 * @author Bokov D.
 */
public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(4, 7);
        Rectangle secondRectangle = new Rectangle(5);
        Rectangle emptyRectangle = new Rectangle();
        Triangle triangle = new Triangle(3, 6);
        Triangle secondTriangle = new Triangle(3);
        Triangle emptyTriangle = new Triangle();

        System.out.println(rectangle.toString());
        System.out.println(secondRectangle.toString() + " (квадрата)" );
        System.out.println(emptyRectangle.toString() + " с значениями по умолчанию");
        System.out.println(triangle.toString());
        System.out.println(secondTriangle.toString() + " (равнобедренного)");
        System.out.println(emptyTriangle.toString() + " c значениями по умолчанию");
    }
}
