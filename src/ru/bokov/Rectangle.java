package ru.bokov;

/**
 * Класс для вычисления площади квадрата и прямоугольника
 * @author Bokov D.
 */
public class Rectangle extends Figure {
    Rectangle(double a, double b){
        this.a=a;
        this.b=b;
    }

    /**
     * Конструктор, инициализирующий квадрат
     *
     * @param a Сторона квадрата
     */
    Rectangle(double a){
        this(a, a);
    }

    /**
     * Коснтурктор, инициализирующий прямоугольник по умолчанию со сторонами (1,1)
     */
    Rectangle(){

        this(1);
    }

    /**
     * Переопределение метода, рассчитывающего площадь прямоугольника
     *
     * @return (a*b) - площадь прямоугольинка
     */
    @Override
    double square(){

        return a*b;
    }

    /**
     * Метод вывода
     */
    @Override
    public String toString(){

        return "Площадь прямоугольника " + square() + " кв. ед";
    }
}
