package ru.bokov;

/**
 * Класс для вычисления площади треугольника
 * @author Bokov D.
 */
public class Triangle extends Figure {
    Triangle(double a, double b){
        this.a=a;
        this.b=b;
    }

    /**
     * Конструктор, инициализирующий равнобедренный прямоугольный треугольник
     *
     * @param a Катет треугольника
     */
    Triangle(double a){

        this(a, a);
    }

    /**
     * Конструктор, инициализирующий прямоугольный треугольник по умолчанию с катетами (1,1)
     *
     */
    Triangle(){

        this(1);
    }

    /**
     * Переопределение метода вычисления площади треугольника
     *
     * @return 0.5*(a*b) - плошадь треугольника
     */
    @Override
    double square(){

        return 0.5*(a*b);
    }

    /**
     * Метод вывода
     */
    @Override
    public String toString(){

        return "Площадь треугольника " + square() + " кв. ед";
    }
}
