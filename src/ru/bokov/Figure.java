package ru.bokov;

/**
 * @author Bokov D.
 */
public abstract class Figure {
    double a;
    double b;
    abstract double square();
}
